package org.prudhvikrishna.messenger.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.prudhvikrishna.messenger.database.DataBaseClass;
import org.prudhvikrishna.messenger.model.*;

public class MessageService {
	
	private Map<Long, Message> messages = DataBaseClass.getMessages();
	
	public MessageService(){
		messages.put(1L, new Message(1L, "Hello World!", "Prudhvi"));
		messages.put(2L, new Message(2L, "Hello Herndon", "Krishna"));
	}
	
	public List<Message> getAllMessages() {
//		Message m1 = new Message(1L, "Hello World!", "Prudhvi");
//		Message m2 = new Message(2L, "Hello Herndon", "Krishna");
//
//		List<Message> messages = new ArrayList<>();
//		messages.add(m1);
//		messages.add(m2);
//
//		return messages;
		
		return new ArrayList<Message>(messages.values());
	}

	public Message getMessage(long id){
		return messages.get(id);
		
	}
	
	public Message addMessage(Message message){
		
		message.setId(messages.size() + 1);
		messages.put(message.getId(), message);
		return message;
	}
	
	public Message updateMessage(Message message){
		if(message.getId() <= 0){
			return null;
		}
		messages.put(message.getId(), message);
		return message;
	}
	
	public Message removeMessage(long id){
		return messages.remove(id);
	}
	
}
